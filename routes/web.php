<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('adddata'));
});

Route::get('/adddata', [App\Http\Controllers\MainController::class, "show"])->name('adddata');
Route::post('/adddata', [App\Http\Controllers\MainController::class, "store"])->name('adddata.store');


Route::get('/report', [App\Http\Controllers\MainController::class, "index"])->name('report');
