<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\Main;
use App\Models\Meters;

use Illuminate\Database\Eloquent\Factories\Factory;

class MainFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Main::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        
            //
            return [
                "dropper_value" => $this->faker->randomFloat(NULL,0,10),
                "dropper_ec" => $this->faker->randomFloat(NULL,0,10),
                "dropper_ph" => $this->faker->randomFloat(NULL,0,10),
                "drainage_value" => $this->faker->randomFloat(NULL,0,10),
                "drainage_ec" => $this->faker->randomFloat(NULL,0,10),
                "drainage_ph" => $this->faker->randomFloat(NULL,0,10),
                "mat_ec" => $this->faker->randomFloat(NULL,0,10),
                "mat_ph" => $this->faker->randomFloat(NULL,0,10),
                "valve" => $this->faker->numberBetween(1,6),
            ];
    }
}
