<?php

namespace Database\Factories;

use App\Models\Meters;
use Illuminate\Database\Eloquent\Factories\Factory;

class MetersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Meters::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            "date" => strtotime(now()), 
            "timefrom" => $this->faker->time($format = 'H:i', $max = 'now'),    
            "timeto" =>  $this->faker->time($format = 'H:i', $max = 'now'), 
            "area" => $this->faker->shuffle(array("1.1", "1.2", "1.4","1.5"))[0],   
            "name" => $this->faker->name,   
        ];
    }
}
