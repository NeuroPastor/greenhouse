<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Meters extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\Models\Meters::factory(50000)
            ->has(\App\Models\Main::factory()->count(6),"meters")
            ->create();
    }
}
