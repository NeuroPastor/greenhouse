<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('mains', function (Blueprint $table) {
        //    $table->id();
            $table->dropColumn('date');
            $table->dropColumn('timefrom');
            $table->dropColumn('timeto');
            $table->dropColumn('area');
            $table->dropColumn('name');
            $table->integer('meterId');
        });
        Schema::create('meterings', function (Blueprint $table) {
            $table->id();
            $table->string('date');
            $table->string('timefrom');
            $table->string('timeto');
            $table->string('area');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
