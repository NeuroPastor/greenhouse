<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mains', function (Blueprint $table) {
            $table->id();
            $table->string('date');
            $table->string('timefrom');
            $table->string('timeto');
            $table->string('area');
            $table->string('name');
            $table->float('dropper_value');
            $table->float('dropper_ec');
            $table->float('dropper_ph');
            $table->float('drainage_value');
            $table->float('drainage_ec');
            $table->float('drainage_ph');
            $table->float('mat_ec');
            $table->float('mat_ph');
            /* 
            *
            * Вообще, сюда бы по хорошему прикрутить авторизацию, возможно SSO, дабы под авторизацией не пришлось заполнять ФИО, да и глаз лишних не было б
            *
            */

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mains');
    }
}
