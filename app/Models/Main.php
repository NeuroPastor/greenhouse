<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Main extends Model
{
    use HasFactory;
    
    protected $fillable = ["date","timefrom","timeto","area","dropper_value","dropper_ec","dropper_ph","drainage_value","drainage_ec","drainage_ph","mat_ec","mat_ph","name"];
}
