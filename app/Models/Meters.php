<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meters extends Model
{
    use HasFactory;

    protected $table = "meterings";

    public function meters()
    {
        return $this->hasMany(\App\Models\Main::class)->orderBy('valve', 'asc');
    }
}
