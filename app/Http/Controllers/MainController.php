<?php

namespace App\Http\Controllers;

use App\Models\Main;
use App\Models\Meters;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $meters = Meters::orderBy('id','DESC')
                    ->with('meters')
                    ->paginate(15);
        foreach($meters as &$m){
            $m->date = date("d.m.Y H:i:s", $m->date);
        }
        return view("report", ['meters'=>$meters]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        print_r($request->dropper[1]);

        $meters = new Meters();
        $meters->date = strtotime($request->input('date'));
        $meters->timefrom = $request->input('timefrom');
        $meters->timeto = $request->input('timeto');
        $meters->area = $request->input('area');
        $meters->name = $request->input('name');

        $meters->save();

        foreach($request->dropper as $k=>$val){ //Именно такой способ, а не while($k<=6) дает возможность добавить новые клапана на фронте, не влезая в непосредственно в бэк
            $valve = new Main();
            $valve->dropper_value = $request->dropper[$k]['v'];
            $valve->dropper_ec = $request->dropper[$k]['ec'];
            $valve->dropper_ph = $request->dropper[$k]['ph'];
            $valve->drainage_value = $request->drainage[$k]['v'];
            $valve->drainage_ec = $request->drainage[$k]['ec'];
            $valve->drainage_ph = $request->drainage[$k]['ph'];
            $valve->mat_ec = $request->mat[$k]['ec'];
            $valve->mat_ph = $request->mat[$k]['ph'];
            $valve->meters_id = $meters->id;
            $valve->valve = $k;
            $valve->save();

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function show(Main $main)
    {
        //
        return view('adddata');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function edit(Main $main)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Main $main)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function destroy(Main $main)
    {
        //
    }
}
