@extends('welcome')

@section('content')
            <div class="row">
                <form action="{{route('adddata')}}" method="POST">
                    @csrf
                    <div class="col-lg-8 offset-lg-2">
                            <div class="row mt-2 mb-2">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <span class="input-group-text" id="addon-wrapping"><b>Дата замеров:</b></span>
                                        <input type="date" name='date' id="date" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <span class="input-group-text"><b>Время замеров:</b></span>
                                        <div class="input-group flex-nowrap">
                                            <span class="input-group-text" for="">C:</span>
                                            <input type="time" name="timefrom" class="form-control" required>
                                            <span class="input-group-text">по:</span>
                                            <input type="time" name="timeto" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2 mb-2">
                                <div class="col-lg-3 text-end align-self-center"><b>Отделение:</b></div>
                                <div class="col-lg-3">
                                    <select name="area" class="form-select">
                                        <option value="1.1">1.1</option>
                                        <option value="1.2">1.2</option>
                                        <option value="1.4">1.4</option>
                                        <option value="1.5">1.5</option>
                                    </select>
                                </div>
                            </div>
                        <table class="table table-hover table-borderless table-sm">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan=3 class="text-center">Капельница</th>
                                    <th colspan=3 class="text-center">Дренаж</th>
                                    <th colspan=2 class="text-center">Мат</th>
                                </tr>
                                <tr>
                                    <th style="width: 190px;">Номер клапана:</th>
                                    <th class="text-center">Объем</th>
                                    <th class="text-center">ЕС</th>
                                    <th class="text-center">рН</th>
                                    <th class="text-center">Объем</th>
                                    <th class="text-center">ЕС</th>
                                    <th class="text-center">рН</th>
                                    <th class="text-center">ЕС</th>
                                    <th class="text-center">рН</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-end">1</td>
                                    <td><input type="number" class="form-control" required name="dropper[1][v]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[1][ec]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[1][ph]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[1][v]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[1][ec]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[1][ph]"></td>
                                    <td><input type="number" class="form-control" required name="mat[1][ec]"></td>
                                    <td><input type="number" class="form-control" required name="mat[1][ph]"></td>
                                </tr>
                                <tr>
                                    <td class="text-end">2</td>
                                    <td><input type="number" class="form-control" required name="dropper[2][v]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[2][ec]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[2][ph]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[2][v]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[2][ec]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[2][ph]"></td>
                                    <td><input type="number" class="form-control" required name="mat[2][ec]"></td>
                                    <td><input type="number" class="form-control" required name="mat[2][ph]"></td>
                                </tr>
                                <tr>
                                    <td class="text-end">3</td>
                                    <td><input type="number" class="form-control" required name="dropper[3][v]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[3][ec]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[3][ph]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[3][v]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[3][ec]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[3][ph]"></td>
                                    <td><input type="number" class="form-control" required name="mat[3][ec]"></td>
                                    <td><input type="number" class="form-control" required name="mat[3][ph]"></td>
                                </tr>
                                <tr>
                                    <td class="text-end">4</td>
                                    <td><input type="number" class="form-control" required name="dropper[4][v]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[4][ec]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[4][ph]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[4][v]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[4][ec]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[4][ph]"></td>
                                    <td><input type="number" class="form-control" required name="mat[4][ec]"></td>
                                    <td><input type="number" class="form-control" required name="mat[4][ph]"></td>
                                </tr>
                                <tr>
                                    <td class="text-end">5</td>
                                    <td><input type="number" class="form-control" required name="dropper[5][v]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[5][ec]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[5][ph]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[5][v]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[5][ec]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[5][ph]"></td>
                                    <td><input type="number" class="form-control" required name="mat[5][ec]"></td>
                                    <td><input type="number" class="form-control" required name="mat[5][ph]"></td>
                                </tr>
                                <tr>
                                    <td class="text-end">6</td>
                                    <td><input type="number" class="form-control" required name="dropper[6][v]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[6][ec]"></td>
                                    <td><input type="number" class="form-control" required name="dropper[6][ph]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[6][v]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[6][ec]"></td>
                                    <td><input type="number" class="form-control" required name="drainage[6][ph]"></td>
                                    <td><input type="number" class="form-control" required name="mat[6][ec]"></td>
                                    <td><input type="number" class="form-control" required name="mat[6][ph]"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-8 offset-lg-2">
                        <div class="row">
                            <div class="input-group  align-items-center">
                                <div class="col">
                                    <label for="name">Замеры выполнил:</label>
                                </div>
                                <div class="col">
                                    <input type="text" name="name" id="name" placeholder="ФИО" class="form-control" required>
                                </div> 
                            </div> 
                        </div>
                    </div>
                    <div class="col-lg-8 offset-lg-2">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>

@endsection