@extends('welcome')

@section('content')
    
    <table class="table table-hover table-borderless table-sm">
        <thead>
            <tr>
                <th>Дата</th>
                <th>Начало</th>
                <th>Конец</th>
                <th>Отделение</th>
                <th>ФИО</th>
            </tr>
        </thead>
        <tbody>
        {{-- @php print_r($meters) @endphp --}}
            @foreach($meters as $m)
                <tr  data-toggle="collapse" href="#meter{{$loop->iteration}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <td>{{$m->date}}</td>
                    <td>{{$m->timefrom}}</td>
                    <td>{{$m->timeto}}</td>
                    <td>{{$m->area}}</td>
                    <td>{{$m->name}}</td>
                </tr>
                <tr class="collapse multi-collapse" id="meter{{$loop->iteration}}">
                    <td  colspan=5>
                        <table class="table-hover table-sm">
                            <thead>
                                <tr>
                                    <th class="text-end">Номер клапана:</th>
                                    <th class="text-center">Объем</th>
                                    <th class="text-center">ЕС</th>
                                    <th class="text-center">рН</th>
                                    <th class="text-center">Объем</th>
                                    <th class="text-center">ЕС</th>
                                    <th class="text-center">рН</th>
                                    <th class="text-center">ЕС</th>
                                    <th class="text-center">рН</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($m->meters as $valve)
                                    <tr>
                                        <td class="text-end">{{$valve->valve}}</td>
                                        <td class="text-center p1">{{$valve->dropper_value}}</td>
                                        <td class="text-center p1">{{$valve->dropper_ec}}</td>
                                        <td class="text-center p1">{{$valve->dropper_ph}}</td>
                                        <td class="text-center p1">{{$valve->drainage_value}}</td>
                                        <td class="text-center p1">{{$valve->drainage_ec}}</td>
                                        <td class="text-center p1">{{$valve->drainage_ph}}</td>
                                        <td class="text-center p1">{{$valve->mat_ec}}</td>
                                        <td class="text-center p1">{{$valve->mat_ph}}</td>
                                    </tr>
                                @endforeach
                                <tr><td colspan="9"><small class="text-danger">Номера клапанов дублируются по замерам т.к. заполнялись (как и остальные данные) наспех "посевом" (seeder)</small></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            @endforeach

        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">{!! $meters->render() !!}</td>
            </tr>
        </tfoot>
    </table>

@endsection